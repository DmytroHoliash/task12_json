package com.holiash.util;

import com.holiash.bankcomparator.BankComparator;
import com.holiash.model.Bank;
import com.holiash.parsers.gson.GsonParser;
import com.holiash.parsers.jackson.JacksonParser;
import com.holiash.parsers.validator.JsonValidator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Menu {

  private Map<Integer, Command> menuCommands;
  private List<String> menu;
  private JsonValidator validator;
  private final String json = "src\\main\\resources\\files\\banks.json";
  private final String writeGson = "src\\main\\resources\\files\\banksGSON.json";
  private final String writeJackson = "src\\main\\resources\\files\\banksJACKSON.json";
  private final String schema = "src\\main\\resources\\files\\banksschema.json";

  public Menu() {
    menu = new ArrayList<>();
    menuCommands = new HashMap<>();
    validator = new JsonValidator();
    initMenu();
    initMenuCommand();
  }

  private void initMenu() {
    menu.add("1 - Test GSON");
    menu.add("2 - Test JACKSON");
    menu.add("0 - EXIT");
  }

  private void initMenuCommand() {
    menuCommands.put(1, this::testGSON);
    menuCommands.put(2, this::testJackson);
  }

  private void testGSON() {
    System.out.println("Testing GSON");
    boolean isValid = validator.validate(json, schema);
    if (isValid) {
      GsonParser gsonParser = new GsonParser();
      Bank[] banks = gsonParser.read(json);
      Arrays.stream(banks).forEach(System.out::println);
      Arrays.sort(banks, new BankComparator());
      gsonParser.write(writeGson, banks);
    }
  }

  private void testJackson() {
    System.out.println("Testing Jackson");
    boolean isValid = validator.validate(json, schema);
    if (isValid) {
      JacksonParser jacksonParser = new JacksonParser();
      Bank[] banks = jacksonParser.read(json);
      Arrays.stream(banks).forEach(System.out::println);
      Arrays.sort(banks, new BankComparator());
      jacksonParser.write(writeJackson, banks);
    }
  }

  public void printMenu() {
    menu.forEach(System.out::println);
  }

  public void execute(int choice) {
    if (this.menuCommands.containsKey(choice)) {
      this.menuCommands.get(choice).run();
    }
  }
}
