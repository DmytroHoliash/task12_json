package com.holiash.model;

public class Address {

  private String city;
  private String street;
  private String number;

  public Address(String city, String street, String streetNumber) {
    this.city = city;
    this.street = street;
    this.number = streetNumber;
  }

  public Address() {
  }

  public void setCity(String city) {
    this.city = city;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getCity() {
    return city;
  }

  public String getStreet() {
    return street;
  }

  public String getNumber() {
    return number;
  }

  @Override
  public String toString() {
    return "Address{" +
      "city='" + city + '\'' +
      ", street='" + street + '\'' +
      ", streetNumber='" + number + '\'' +
      '}';
  }
}
