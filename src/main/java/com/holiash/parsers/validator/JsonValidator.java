package com.holiash.parsers.validator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JsonValidator {

  public boolean validate(String filePath, String schemaPath) {
    try (InputStream schemaStream = new FileInputStream(new File(schemaPath));
      InputStream fileStream = new FileInputStream(new File(filePath))) {
      JSONArray file = new JSONArray(new JSONTokener(fileStream));
      JSONObject rawSchema = new JSONObject(new JSONTokener(schemaStream));
      Schema schema = SchemaLoader.load(rawSchema);
      schema.validate(file);
      return true;
    } catch (IOException | ValidationException e) {
      System.out.println(e.getMessage());
    }
    return false;
  }
}
