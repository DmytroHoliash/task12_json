package com.holiash.parsers.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.holiash.model.Bank;
import java.io.File;
import java.io.IOException;

public class JacksonParser {

  private final ObjectMapper mapper = new ObjectMapper();

  public Bank[] read(String path) {
    File from = new File(path);
    Bank[] banks = new Bank[0];
    try {
      banks = mapper.readValue(from, Bank[].class);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
    return banks;
  }

  public void write(String path, Bank[] banks) {
    File to = new File(path);
    try {
      mapper.writeValue(to, banks);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }
}
