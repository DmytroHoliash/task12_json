package com.holiash.parsers.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.holiash.model.Bank;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.NoSuchElementException;

public class GsonParser {

  public Bank[] read(String path) {
    Gson gson = new Gson();
    JsonReader reader = null;
    try {
      reader = new JsonReader(new FileReader(path));
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }
    if (reader != null) {
      Bank[] banks = gson.fromJson(reader, Bank[].class);
      return banks;
    }
    throw new NoSuchElementException();
  }

  public void write(String path, Bank[] banks) {
    Gson gson = gson = new GsonBuilder().setPrettyPrinting().create();
    String str = gson.toJson(banks);
    try(FileWriter writer = new FileWriter(path)) {
      writer.write(str);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }
}
